# civalgo-merge-bot

To make this work, you must:
- Create a auth token for the bot account and create a file name "marge-token" containing the token
- Create a ssh-key combinaison name "marge-key" and add the public key to the bot account
- Grant maintainer or admin permissions on repos for the bot account
- Assign merge requests to the bot account to merge it when they are ready.
